/*
 * Copyright 2013 Li-Yuchong <liyuchong@cyanclone.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SURF_C_Painters_h
#define SURF_C_Painters_h

#include "Image.h"

#include <stdlib.h>
#include <math.h>

#define COLOR_RED    255
#define COLOR_GREEN  65280
#define COLOR_BLUE   16711680
#define COLOR_YELLOW 65535
#define COLOR_CYAN   16776960

#define LINEAR_FILTER(h, l, a) ((l) + (((h) - (l)) * (a)))

#define LINEAR_FILTER_COLOR(c1,c2,a)                                    \
    RGB(LINEAR_FILTER(GETR(c1), GETR(c2), (a)), LINEAR_FILTER(GETG(c1), \
    GETG(c2), (a)), LINEAR_FILTER(GETB(c1), GETB(c2), (a)))

#define RGB(r,g,b)  (int32_t)((((unsigned char)(r)                      \
                    | ((unsigned short)((unsigned char)(g)) << 8))      \
                    | (((unsigned long)(unsigned char)(b)) << 16)))

#define GETR(c)   ((c) & 0x0000FF)
#define GETG(c)  (((c) & 0x00FF00) >> 8)
#define GETB(c)  (((c) & 0xFF0000) >> 16)

#define GET_IMAGE_PIXEL(__x, __y)                             \
    RGB((image)->pixel[bytesPerLine * (__y) + (__x) * 4],     \
        (image)->pixel[bytesPerLine * (__y) + (__x) * 4 + 1], \
        (image)->pixel[bytesPerLine * (__y) + (__x) * 4 + 2])

#define SET_IMAGE_PIXEL(__x, __y, __c)                                \
do {                                                                  \
    (image)->pixel[bytesPerLine * (__y) + (__x) * 4]     = GETR(__c); \
    (image)->pixel[bytesPerLine * (__y) + (__x) * 4 + 1] = GETG(__c); \
    (image)->pixel[bytesPerLine * (__y) + (__x) * 4 + 2] = GETB(__c); \
} while(0)

typedef uint8_t BYTE;

void paintImageTo(const Image *image, Image *target, int x, int y);
void drawCircle(Image *image, int cx, int cy, int r, int value);
void drawLine(Image *image, int x0, int y0, int x1, int y1, int color);

#endif
